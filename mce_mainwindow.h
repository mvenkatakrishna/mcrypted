/***********************************************************************
* Copyright (C) 2019  Mahesh Venkata Krishna (mahesh@maheshvk.com)     *
*                                                                      *
* This program is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by *
* the Free Software Foundation, either version 3 of the License, or    *
* (at your option) any later version.                                  *
*                                                                      *
* This program is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of       *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
* GNU General Public License for more details.                         *
*                                                                      *
* You should have received a copy of the GNU General Public License    *
* along with this program. If not, see <https://www.gnu.org/licenses/> *
***********************************************************************/

#ifndef MCE_MAINWINDOW_H
#define MCE_MAINWINDOW_H

#include <QVector>
#include <QMainWindow>
#include <QSettings>
#include <QLabel>
#include <QTextEdit>
#include <QTextBrowser>
#include <QCryptographicHash>

#include "mce_finddialog.h"
#include "mce_colortheme.h"

#define CASTINT8(x)   static_cast<int8_t>(x)
#define CASTUINT32(x) static_cast<uint32_t>(x)
#define CASTUINT64(x) static_cast<uint64_t>(x)
#define CASTUINT8P(x) reinterpret_cast<uint8_t*>(x)

namespace Ui {
class MCE_MainWindow;
}

struct MCE_TabInfo
{
    QString fName = "";
    bool isSaved = true;
    bool isFirstLoad = true;
    QTextEdit * widget = nullptr;
    QByteArray key = QByteArray( 32, '0' );
    QByteArray nonce = QByteArray( 32, '0' );
};

class MCE_MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MCE_MainWindow(QWidget *parent = nullptr);
    ~MCE_MainWindow();

    bool eventFilter(QObject * obj, QEvent * e);

private slots:
    void on_actionQuit_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_triggered();

    void on_actionNew_triggered();

    void on_actionHelp_triggered();

    void on_actionAbout_triggered();

    void on_tabWidget_tabBarDoubleClicked(int index);

    void on_actionSave_All_triggered();

    void onTabCloseRequested(int index);

    void onTabMoved(int from, int to);

    void onTextChanged();

    void on_actionUndo_triggered();

    void on_actionRedo_triggered();

    void on_actionSelect_All_triggered();

    void on_actionToggle_Line_Wrap_triggered();

    void on_actionFind_triggered();

    void on_actionZoom_In_triggered();

    void on_actionZoom_Out_triggered();

    void onFindBtnClicked();

    void onFindDlgClosed();

    void on_actionCut_triggered();

    void on_actionCopy_triggered();

    void on_actionPaste_triggered();

    void on_actionToolbar_triggered();

    void on_actionClose_Tab_triggered();

    void on_actionSave_As_triggered();

    void onCursorPosChanged();

    void on_actionTheme_Preferences_triggered();

    void onThemeSet();

    void on_actionPrint_triggered();

private:
    void closeEvent(QCloseEvent * e);

    void m_addNewTab();

    void m_saveTab(int idx);

    QTextCursor m_findText();

    void m_getNonce( QByteArray & str );

    inline QByteArray m_getKeyString( QByteArray pwd );

    inline bool m_getPassword( QByteArray & pwd);

    void m_setThemeAndSync();

    QSettings * m_settings;

    MCE_Theme m_theme;

    const QByteArray m_headerStr = QCryptographicHash::hash("MCrypted encrypted text file", QCryptographicHash::Md5);

    const QString m_aboutTxt = "<center><img src='mce_icon.png' alt='MCryptEd Logo' height=60 width=45 /></center>"
                             "<h1 align=center>MCryptEd</h1><br><br>"
                             "<p align=center><b><font size='4'>Version: Alpha</font></b></p>"
                             "<p align=center><b><font size='3'>Copyright &#169; Mahesh Venkata Krishna "
                             "(<a href='mailto:mahesh@maheshvk.com'>mahesh@maheshvk.com</a>)</font></b></p><br>"
                             "<p align=center>This program is free software: you can redistribute it and/or modify it under the terms of the "
                             "GNU General Public License as published by the Free Software Foundation, either version 3 of the License, "
                             "or(at your option) any later version.</p>"
                             "<p align=center>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the "
                             "implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.</p>"
                             "<p align=center>You should have received a copy of the GNU General Public License along with this program. "
                             " If not, see <a href='https://www.gnu.org/licenses/gpl-3.0.en.html'>https://www.gnu.org/licenses/gpl-3.0.en.html</a>.</p><br>"
                             "<p align=center>This software is built using the <a href='https://www.qt.io/'>Qt Library</a> under GNU LGPL v3.</p><br>"
                             "<p align=center>The AES-256 cryptography in this software uses <a href='https://github.com/kokke/tiny-AES-c'>tiny-AES-c</a>.</p><br>"
                             "<p align=center>Most of the icons in this program come from the "
                             "<a href='https://github.com/EmptyStackExn/mono-dark-flattr-icons'>mono-dark-flattr</a> icon theme distributed under CC-BY-SA-4.0 license.</p>";

    const QString m_helpTxt = "<h1>MCryptEd Help</h1>"
                            "<p>MCryptEd is an editor featuring encryption/decryption mechanism when opening/saving documents. This is useful for keeping "
                            "sensitive information like personal financial details, drafts of sensitive text documts etc. in your computer without exposing "
                            "them to unauthorized eyes. It uses AES-256 CTR cryptography scheme, which is approved by NIST as being sufficient for classified "
                            "documents.</p>"
                            "<h2>Usage</h2>"
                            "<p>The standard workflow is like that of any text editor, but with the difference that while saving, the program asks for a password. "
                            "This would then be used in the encryption. Also, the encrypted file is appended with a header containing the non-secret nonce. "
                            "This is saved then as a simple UTF-8 encoded text file in the disk. When opening again, you need to re-enter the password to be able "
                            "to read the decrypted text. It is important that you can remember or securely store the password elsewhere.</p>"
                            "";


    QLabel * m_msgLabel;
    QLabel * m_psnLabel;

    QVector<MCE_TabInfo> m_tabInfo;
    Ui::MCE_MainWindow *ui;
    QTabBar * m_tabBar;
    QTextBrowser m_about;
    QTextBrowser m_help;

    MCE_FindDialog * m_findDlg;
    MCE_FindInfo m_findInfo;
    bool m_toolBarVisible;

    MCE_ColorTheme * m_themeDlg;
};

#endif // MCE_MAINWINDOW_H
