#include "mce_colortheme.h"
#include "ui_mce_colortheme.h"

#include <QColorDialog>
#include <QColor>
#include <QRegularExpression>

MCE_ColorTheme::MCE_ColorTheme(QWidget *parent, MCE_Theme theme) :
    QWidget(parent),
    ui(new Ui::colorTheme)
{
    ui->setupUi(this);

    m_theme = theme;

    m_setSampleTheme();
}

MCE_ColorTheme::~MCE_ColorTheme()
{
    delete ui;
}

MCE_Theme MCE_ColorTheme::getTheme()
{
    return m_theme;
}

void MCE_ColorTheme::on_okBtn_clicked()
{
    emit themeSelected();
}

void MCE_ColorTheme::on_cancelBtn_clicked()
{
    this->close();
}

void MCE_ColorTheme::on_clrBtn1_clicked()
{
    QColor clr = QColorDialog::getColor(QColor(m_theme.bgndClr), nullptr, "Pick Color");
    if(!clr.isValid())
        return;

    QRegularExpression rex("^#[0-9a-f]{6}$");
    QString tmp = clr.name(QColor::HexRgb);
    QRegularExpressionMatch res = rex.match(tmp);
    if(!res.hasMatch())
        return;
    m_theme.bgndClr = tmp;
    ui->bgndEdit->setText(m_theme.bgndClr);
    m_setSampleTheme();
}

void MCE_ColorTheme::on_clrBtn2_clicked()
{
    QColor clr = QColorDialog::getColor(QColor(m_theme.fgndClr), nullptr, "Pick Color");
    if(!clr.isValid())
        return;

    QRegularExpression rex("^#[0-9a-f]{6}$");
    QString tmp = clr.name(QColor::HexRgb);
    QRegularExpressionMatch res = rex.match(tmp);
    if(!res.hasMatch())
        return;
    m_theme.fgndClr = tmp;
    ui->fgndEdit->setText(m_theme.fgndClr);
    m_setSampleTheme();
}

void MCE_ColorTheme::on_clrBtn3_clicked()
{
    QColor clr = QColorDialog::getColor(QColor(m_theme.selBClr), nullptr, "Pick Color");
    if(!clr.isValid())
        return;

    QRegularExpression rex("^#[0-9a-f]{6}$");
    QString tmp = clr.name(QColor::HexRgb);
    QRegularExpressionMatch res = rex.match(tmp);
    if(!res.hasMatch())
        return;
    m_theme.selBClr = tmp;
    ui->selbEdit->setText(m_theme.selBClr);
    m_setSampleTheme();
}

void MCE_ColorTheme::on_fontBox_currentFontChanged(const QFont &f)
{
    m_theme.fntName = f.family();
    m_setSampleTheme();
}

void MCE_ColorTheme::on_sizeBox_valueChanged(int size)
{
    m_theme.fntSize = QString("%1").arg(size);
    m_setSampleTheme();
}

void MCE_ColorTheme::on_styleBox_currentIndexChanged(const QString &style)
{
    m_theme.fntStyle = style.toLower();
    m_setSampleTheme();
}

void MCE_ColorTheme::on_defaultBtn_clicked()
{
    m_theme = MCE_Theme();
    m_setSampleTheme();
}

void MCE_ColorTheme::m_setSampleTheme()
{
    ui->bgndEdit->setText(m_theme.bgndClr);
    ui->fgndEdit->setText(m_theme.fgndClr);
    ui->selbEdit->setText(m_theme.selBClr);
    ui->fontBox->setCurrentFont(QFont(m_theme.fntName));
    ui->sizeBox->setValue(m_theme.fntSize.toUInt());
    ui->styleBox->setCurrentText(m_theme.fntStyle);
    ui->trialEdit->setStyleSheet(m_theme.styleString());
}
