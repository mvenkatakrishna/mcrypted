#-------------------------------------------------
#
# Project created by QtCreator 2019-02-27T18:07:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = MCryptEd
TEMPLATE = app

CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
    mce_colortheme.cpp \
    mce_finddialog.cpp \
        mce_mainwindow.cpp \
    tiny_AES/aes.c

HEADERS += \
    mce_colortheme.h \
    mce_finddialog.h \
        mce_mainwindow.h \
    tiny_AES/aes.h

FORMS += \
    mce_colortheme.ui \
    mce_finddialog.ui \
        mce_mainwindow.ui

RESOURCES += \
    mce_resources.qrc
