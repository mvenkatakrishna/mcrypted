# MCryptEd - Secure Editor

This is MCrypted, a simple "secure" text editor. Basic usage is the same as any editor, but when you save a file, it asks for a password. If you give a password, it encrypts the file with that password and saves the encrypted text. When you open such a file again, you will be asked for the password.

## Security

How "secure" is MCryptEd? Well, for starters, it is not meant to be used for storing data from military research. It is meant to store stuff like some passwords, PINs, notes, money details, financial plans and calculations etc. in such a way that a casual person opening the file will not be able to view the data. Since it uses AES256-CTR encryption, it is actually quite secure even for classified documents, provided you supply a good password. However, it doesn't store the data in secure RAM locations while the file is being edited, and is therefore not immune to Hardware based attacks or attacks based on accessing memory while the file is open in the editor.

## Password Selection

I recommend having different passwords for different files and storing them in a password manager you trust, such as [Keepass](https://keepass.info/), [PasswordSafe](https://www.pwsafe.org/) or [UPM](http://upm.sourceforge.net/). I recommend even using these programs to generate good passwords for you. Beware, if you loose the password, there is no way of recovering the data from the file.
