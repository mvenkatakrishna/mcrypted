/***********************************************************************
* Copyright (C) 2019  Mahesh Venkata Krishna (mahesh@maheshvk.com)   *
*                                                                      *
* This program is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by *
* the Free Software Foundation, either version 3 of the License, or    *
* (at your option) any later version.                                  *
*                                                                      *
* This program is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of       *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
* GNU General Public License for more details.                         *
*                                                                      *
* You should have received a copy of the GNU General Public License    *
* along with this program. If not, see <https://www.gnu.org/licenses/> *
***********************************************************************/
#include "mce_finddialog.h"
#include "ui_mce_finddialog.h"

MCE_FindDialog::MCE_FindDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FindDialog)
{
    ui->setupUi(this);
}

MCE_FindDialog::~MCE_FindDialog()
{
    delete ui;
}

MCE_FindInfo MCE_FindDialog::getInfo()
{
    return m_info;
}

void MCE_FindDialog::on_closeBtn_clicked()
{
    emit findDlgClosed();
    this->close();
}

void MCE_FindDialog::on_nextBtn_clicked()
{
    m_info.btn = NEXT;
    m_readGuiInfo();
    emit btnClicked();
}

void MCE_FindDialog::on_replaceBtn_clicked()
{
    m_info.btn = REPLACE;
    m_readGuiInfo();
    emit btnClicked();
}

void MCE_FindDialog::on_repAllBtn_clicked()
{
    m_info.btn = REPLACEALL;
    m_readGuiInfo();
    emit btnClicked();
}

inline void MCE_FindDialog::m_readGuiInfo()
{
    m_info.find = ui->findEdit->text();
    m_info.replace = ui->replaceEdit->text();
    m_info.caseSensitive = ui->caseBox->isChecked();
    m_info.matchWord = ui->wordBox->isChecked();
    m_info.backwards = ui->backBox->isChecked();
    m_info.regex = ui->regexBox->isChecked();
}
