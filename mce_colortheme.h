#ifndef COLORTHEME_H
#define COLORTHEME_H

#include <QWidget>

namespace Ui {
class colorTheme;
}

struct MCE_Theme
{
    QString themeName = "Default";
    QString bgndClr   = "#282828"; // Background color
    QString fgndClr   = "#dcdcdc"; // Text color
    QString selBClr   = "#0055ff"; // Selection background color
    QString fntName   = "Times New Roman"; // Font name
    QString fntSize   = "12";      // Font size in pt
    QString fntStyle  = "normal"; // Font style

    QString styleString()
    {
        return "background: " + bgndClr + "; color: " + fgndClr +
                 "; selection-background-color: " + selBClr +
                 "; font-family: \"" + fntName + "\"; font-size: " + fntSize +
                 "pt; font-style: " + fntStyle;
    }
};

class MCE_ColorTheme : public QWidget
{
    Q_OBJECT

public:
    explicit MCE_ColorTheme(QWidget *parent = nullptr, MCE_Theme theme = MCE_Theme());
    ~MCE_ColorTheme();

    MCE_Theme getTheme();

private slots:
    void on_okBtn_clicked();

    void on_cancelBtn_clicked();

    void on_clrBtn1_clicked();

    void on_clrBtn2_clicked();

    void on_defaultBtn_clicked();

    void on_clrBtn3_clicked();

    void on_fontBox_currentFontChanged(const QFont &f);

    void on_styleBox_currentIndexChanged(const QString &style);

    void on_sizeBox_valueChanged(int size);

signals:
    void themeSelected();

private:
    void m_setSampleTheme();

    Ui::colorTheme *ui;

    MCE_Theme m_theme;
};

#endif // COLORTHEME_H
