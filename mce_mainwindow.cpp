/***********************************************************************
* Copyright (C) 2019  Mahesh Venkata Krishna (mahesh@maheshvk.com)     *
*                                                                      *
* This program is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by *
* the Free Software Foundation, either version 3 of the License, or    *
* (at your option) any later version.                                  *
*                                                                      *
* This program is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of       *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
* GNU General Public License for more details.                         *
*                                                                      *
* You should have received a copy of the GNU General Public License    *
* along with this program. If not, see <https://www.gnu.org/licenses/> *
***********************************************************************/

#include "mce_mainwindow.h"
#include "ui_mce_mainwindow.h"

#define CBC 0
#define ECB 0
extern "C"
{
#include "tiny_AES/aes.h"
}

#include <random>
#include <stdint.h>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>
#include <QMouseEvent>
#include <QByteArray>
#include <QMessageBox>
#include <QDateTime>
#include <QInputDialog>
#include <QStandardPaths>
#include <QTextBlock>
#include <QTabBar>
#include <QRegularExpression>
#include <QSettings>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>

#include <QDebug>


MCE_MainWindow::MCE_MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MCE_MainWindow)
{
    ui->setupUi(this);

    m_theme = MCE_Theme();
    m_settings = new QSettings("settings.ini", QSettings::IniFormat);
    m_settings->sync();
    m_settings->beginGroup("theme");
    QStringList keys = m_settings->allKeys();
    if(keys.contains("bgnd_color"))
        m_theme.bgndClr = m_settings->value("bgnd_color").toString();
    if(keys.contains("fgnd_color"))
        m_theme.fgndClr = m_settings->value("fgnd_color").toString();
    if(keys.contains("selb_color"))
        m_theme.selBClr = m_settings->value("selb_color").toString();
    if(keys.contains("font_name"))
        m_theme.fntName = m_settings->value("font_name").toString();
    if(keys.contains("font_size"))
        m_theme.fntSize = m_settings->value("font_size").toString();
    if(keys.contains("font_style"))
        m_theme.fntStyle = m_settings->value("font_style").toString();
    m_settings->endGroup();
    m_settings->sync();

    m_msgLabel = new QLabel();
    m_msgLabel->setTextFormat(Qt::RichText);
    m_msgLabel->setText("<font color='black'>&nbsp;&nbsp;&nbsp;Ready</font>");
    ui->statusBar->addWidget(m_msgLabel, 2);

    m_psnLabel = new QLabel(QString("Line: %1 Column: %2").arg(1).arg(1));
    ui->statusBar->addWidget(m_psnLabel, 1);

    QIcon::setThemeName("breeze-light");


    // Prepare tabs
    m_tabBar = ui->tabWidget->tabBar();
    m_addNewTab();
    ui->tabWidget->installEventFilter(this);
    connect( m_tabBar, &QTabBar::tabCloseRequested, this, &MCE_MainWindow::onTabCloseRequested );
    connect( m_tabBar, &QTabBar::tabMoved, this, &MCE_MainWindow::onTabMoved );

    m_findDlg = new MCE_FindDialog();
    connect( m_findDlg, &MCE_FindDialog::btnClicked, this, &MCE_MainWindow::onFindBtnClicked );
    connect( m_findDlg, &MCE_FindDialog::findDlgClosed, this, &MCE_MainWindow::onFindDlgClosed );

    m_themeDlg = new MCE_ColorTheme(nullptr, m_theme);
    connect(m_themeDlg, &MCE_ColorTheme::themeSelected, this, &MCE_MainWindow::onThemeSet);

    // Prepare the texts for help and about
    m_about.resize(640, 480);
    m_about.setHtml(m_aboutTxt);
    m_help.resize(640, 480);
    m_help.setHtml(m_helpTxt);

    m_toolBarVisible = true;
}

MCE_MainWindow::~MCE_MainWindow()
{
    int n = m_tabInfo.size();
    for(int i = 0; i < n; ++i )
    {
        if( m_tabInfo[i].widget == nullptr )
            continue;
        delete m_tabInfo[i].widget;
    }
    delete m_findDlg;
    delete m_themeDlg;
    m_settings->sync();
    delete m_settings;
    delete ui;
}

void MCE_MainWindow::on_actionQuit_triggered()
{
    this->close();
}

void MCE_MainWindow::closeEvent(QCloseEvent * e)
{
    bool isNotSaved = false;
    for(auto info: m_tabInfo)
    {
        if(!info.isSaved)
        {
            isNotSaved = true;
            break;
        }
    }
    if(isNotSaved)
    {
        QMessageBox::StandardButton ret = QMessageBox::question(
                                       this, "Confirm Close",
                                       "Some tabs are not saved. Do you want to close the program? "
                                       "(Any changes made since last save will be lost)",
                                       (QMessageBox::Close|QMessageBox::Cancel), QMessageBox::Cancel );
        if(ret == QMessageBox::Cancel)
        {
            e->ignore();
            return;
        }
    }

    e->accept();
}

bool MCE_MainWindow::eventFilter(QObject * obj, QEvent * e)
{
    if( (obj == ui->tabWidget) && (e->type() == QEvent::MouseButtonDblClick) )
    {
        QRect tabbar_g = m_tabBar->geometry();
        QRect tabwidget_g = ui->tabWidget->geometry();
        QRect empty_g( (tabbar_g.x()+tabbar_g.width()), tabbar_g.y(), tabwidget_g.width(), tabbar_g.height());

        QMouseEvent * m_e = static_cast<QMouseEvent *>(e);
        if( empty_g.contains(m_e->pos()) )
            m_addNewTab();
    }
    return QObject::eventFilter(obj, e);
}

void MCE_MainWindow::on_actionOpen_triggered()
{
    QString fName = QFileDialog::getOpenFileName(
                             this, "Open File",
                             QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation)[0],
                             "Text(*.txt)");

    bool alreadyExists = false;
    for( auto& info: m_tabInfo )
    {
        if(info.fName == fName)
        {
            alreadyExists = true;
            ui->tabWidget->setCurrentWidget(info.widget);
            if(info.isSaved)
                return;
            else
            {
                QMessageBox::StandardButton ret = QMessageBox::question(
                         this,
                         "Confirm Reload",
                         "This file is already open. Reload? (Any changes made since last save will be lost)",
                         (QMessageBox::Yes|QMessageBox::No),
                         QMessageBox::No );
                if(ret == QMessageBox::No)
                    return;
                else
                    break;
            }
        }
    }

    QFile f(fName);
    if( !f.open(QIODevice::ReadOnly) )
        return;
    QByteArray txt = f.readAll();
    f.close();
    if(!alreadyExists)
        on_actionNew_triggered();

    int idx = m_tabBar->currentIndex();

    if(txt.left(16) != m_headerStr)
    {
        m_tabInfo[ui->tabWidget->currentIndex()].widget->setPlainText( txt );
        m_msgLabel->setText("<font color='black'>&nbsp;&nbsp;&nbsp;Files opened</font>");
        return;
    }

    if( m_tabInfo[idx].key == QByteArray(32, '0') )
    {
        QByteArray pwd;
        bool ok = m_getPassword(pwd);
        if( !ok || (pwd == "") )
        {
            m_tabInfo[ui->tabWidget->currentIndex()].widget->setPlainText( txt );
            m_msgLabel->setText("<font color='black'>&nbsp;&nbsp;&nbsp;File opened</font>");
            return;
        }
        m_tabInfo[idx].key = m_getKeyString(pwd);
    }
    // Remove the header string
    txt = txt.remove(0, 16);
    m_tabInfo[idx].nonce = txt.left(32);
    // Remove the nonce from the stream
    txt = txt.remove(0, 32);

    // Decrypt the text
    AES_ctx ctx;
    AES_init_ctx_iv(&ctx, CASTUINT8P(m_tabInfo[idx].key.data()), CASTUINT8P(m_tabInfo[idx].nonce.data()));
    AES_CTR_xcrypt_buffer(&ctx, CASTUINT8P(txt.data()), CASTUINT32(txt.size()));
    // Set the text now
    m_tabInfo[idx].widget->setPlainText( txt );
    m_msgLabel->setText("<font color='black'>&nbsp;&nbsp;&nbsp;File opened</font>");
    m_tabInfo[idx].fName = fName;
    m_tabInfo[idx].isSaved = true;
    QFileInfo info(fName);
    m_tabBar->setTabText(idx, info.baseName());
    m_tabBar->setTabToolTip(idx, info.absoluteFilePath());
}

void MCE_MainWindow::on_actionNew_triggered()
{
    m_addNewTab();
}

void MCE_MainWindow::m_addNewTab()
{
    QTextEdit * edit = new QTextEdit();
    edit->setStyleSheet(m_theme.styleString());
    MCE_TabInfo info;
    info.widget = edit;
    m_tabInfo.append(info);
    int idx = m_tabBar->count();
    ui->tabWidget->addTab( m_tabInfo.last().widget, QString("New_%1.txt").arg(idx) );
    ui->tabWidget->setCurrentWidget( m_tabInfo.last().widget );
    connect(m_tabInfo.last().widget, SIGNAL(textChanged()), this, SLOT(onTextChanged()));
    connect(m_tabInfo.last().widget, SIGNAL(cursorPositionChanged()), this, SLOT(onCursorPosChanged()));
}

void MCE_MainWindow::onCursorPosChanged()
{
    if(m_tabInfo[m_tabBar->currentIndex()].widget->lineWrapMode() != QTextEdit::NoWrap)
    {
        m_psnLabel->setText( QString("Line: %1 Column: %2").arg(0).arg(0) );
        return;
    }
    QTextCursor c = m_tabInfo[m_tabBar->currentIndex()].widget->textCursor();
    int lineNum = c.block().layout()->lineForTextPosition(c.position()).lineNumber();
    for (QTextBlock block = c.block().previous(); block.isValid(); block = block.previous())
        lineNum += block.lineCount();

    m_psnLabel->setText( QString("Line: %1 Column: %2").arg(lineNum+1).arg(c.columnNumber()+1) );
}

void MCE_MainWindow::on_tabWidget_tabBarDoubleClicked(int index)
{
    if( index < 0 )
        m_addNewTab();
    else
        onTabCloseRequested(index);
}

void MCE_MainWindow::onTabCloseRequested(int index)
{
    // Check whether the file is saved.
    if(m_tabInfo[index].isSaved == false)
    {
        QMessageBox::StandardButton ret = QMessageBox::question(
                this,
                "Confirm Close",
                "This file is not saved. Do you want to close it? (Any changes made since last save will be lost)",
                (QMessageBox::Close|QMessageBox::Save|QMessageBox::Cancel),
                QMessageBox::Cancel );
        if(ret == QMessageBox::Cancel)
            return;
        if(ret == QMessageBox::Save)
            m_saveTab(index);
    }

    QTextEdit * ed = m_tabInfo.takeAt(index).widget;

    if( ed == nullptr )
        return;
    delete ed;
}

void MCE_MainWindow::on_actionClose_Tab_triggered()
{
    onTabCloseRequested(m_tabBar->currentIndex());
}

void MCE_MainWindow::on_actionSave_triggered()
{
    int idx = ui->tabWidget->currentIndex();
    if(!m_tabInfo[idx].isSaved || (m_tabInfo[idx].fName == ""))
        m_saveTab( idx );
}

void MCE_MainWindow::on_actionSave_All_triggered()
{
    int idx = m_tabBar->currentIndex();
    for(int i = 0; i < m_tabInfo.size(); ++i)
    {
        m_tabBar->setCurrentIndex(i);
        if(!m_tabInfo[i].isSaved)
            m_saveTab(i);
    }
    m_tabBar->setCurrentIndex(idx);
}

void MCE_MainWindow::on_actionSave_As_triggered()
{
    int idx = m_tabBar->currentIndex();
    QString fName = QFileDialog::getSaveFileName(this, "Save", m_tabBar->tabText(idx), "Text(*.txt)");
    if(fName != "")
    {
        m_tabInfo[idx].fName = fName;
        m_saveTab(idx);
    }
}

void MCE_MainWindow::m_saveTab(int idx)
{
    if( (idx < 0) || (m_tabInfo.size() <= idx) )
        return;

    if(m_tabInfo[idx].fName == "")
        m_tabInfo[idx].fName = QFileDialog::getSaveFileName(this, "Save", m_tabBar->tabText(idx), "Text(*.txt)");

    QFile oFile(m_tabInfo[idx].fName);
    if( !oFile.open(QFile::WriteOnly) )
        return;

    QFileInfo info(m_tabInfo[idx].fName);
    m_tabBar->setTabText(idx, info.fileName());
    m_tabBar->setTabToolTip(idx, info.absoluteFilePath());

    if( m_tabInfo[idx].nonce == QByteArray( 32, '0' ) )
        m_getNonce(m_tabInfo[idx].nonce);

    // Get password
    if( m_tabInfo[idx].key ==  QByteArray( 32, '0' )  )
    {
        QByteArray pwd;
        bool ok = m_getPassword(pwd);
        if(ok && (pwd != ""))
            m_tabInfo[idx].key = m_getKeyString(pwd);
    }

    QByteArray content = m_tabInfo[m_tabBar->currentIndex()].widget->toPlainText().toUtf8();

    if( m_tabInfo[idx].key !=  QByteArray( 32, '0' ) )
    {
        AES_ctx ctx;
        AES_init_ctx_iv(&ctx, CASTUINT8P(m_tabInfo[idx].key.data()), CASTUINT8P(m_tabInfo[idx].nonce.data()));
        AES_CTR_xcrypt_buffer(&ctx, CASTUINT8P(content.data()), CASTUINT32(content.size()));
        oFile.write(m_headerStr);
        oFile.write(m_tabInfo[idx].nonce);
    }
    oFile.write(content);
    oFile.close();
    m_tabInfo[idx].isSaved = true;
    m_tabBar->setTabTextColor(idx, QColor());
    m_msgLabel->setText("<font color='black'>&nbsp;&nbsp;&nbsp;Saved file</font>");
}

void MCE_MainWindow::onTabMoved(int from, int to)
{
    if((from >= 0) || (from < m_tabInfo.size()) ||
       (to >= 0)   || (to < m_tabInfo.size())     )
        m_tabInfo.move(from, to);
}

void MCE_MainWindow::onTextChanged()
{
    int idx = m_tabBar->currentIndex();
    if(m_tabInfo[idx].isFirstLoad)
    {
        m_tabInfo[idx].isFirstLoad = false;
        return;
    }
    m_tabInfo[idx].isSaved = false;
    m_tabBar->setTabTextColor(idx, QColor(255,10,10));
}

void MCE_MainWindow::on_actionUndo_triggered()
{
    m_tabInfo[m_tabBar->currentIndex()].widget->undo();
}

void MCE_MainWindow::on_actionRedo_triggered()
{
    m_tabInfo[m_tabBar->currentIndex()].widget->redo();
}

void MCE_MainWindow::on_actionToggle_Line_Wrap_triggered()
{
    QTextEdit * ed = m_tabInfo[m_tabBar->currentIndex()].widget;
    if(ed->lineWrapMode() == QTextEdit::NoWrap)
        ed->setLineWrapMode(QTextEdit::WidgetWidth);
    else
        ed->setLineWrapMode(QTextEdit::NoWrap);
    onCursorPosChanged();
}

void MCE_MainWindow::on_actionZoom_In_triggered()
{
    m_tabInfo[m_tabBar->currentIndex()].widget->zoomIn();
}

void MCE_MainWindow::on_actionZoom_Out_triggered()
{
    m_tabInfo[m_tabBar->currentIndex()].widget->zoomOut();
}

void MCE_MainWindow::on_actionFind_triggered()
{
    m_findDlg->show();
}

void MCE_MainWindow::onFindBtnClicked()
{
    m_findInfo = m_findDlg->getInfo();
    QTextEdit * ed = m_tabInfo[m_tabBar->currentIndex()].widget;
    QTextCursor c = m_findText();

    if ( (c != QTextCursor()) && (m_findInfo.btn == REPLACEALL))
    {
        while( c != QTextCursor() )
        {
            ed->setTextCursor(c);
            ed->textCursor().removeSelectedText();
            ed->textCursor().insertText(m_findInfo.replace);
        }
        return;
    }

    if( c != QTextCursor() )
        ed->setTextCursor(c);
    else
    {
        m_msgLabel->setText("<font color='red'>&nbsp;&nbsp;&nbsp;No match found!</font>");
        return;
    }

    if(m_findInfo.btn == REPLACE)
    {
        ed->textCursor().removeSelectedText();
        ed->textCursor().insertText(m_findInfo.replace);
    }
}

void MCE_MainWindow::onFindDlgClosed()
{
    m_msgLabel->setText("<font color='black'>&nbsp;&nbsp;&nbsp;Ready</font>");
}

QTextCursor MCE_MainWindow::m_findText()
{
    QTextEdit * ed = m_tabInfo[m_tabBar->currentIndex()].widget;
    QFlags<QTextDocument::FindFlag> flag = nullptr;
    if (m_findInfo.caseSensitive)
        flag |= QTextDocument::FindCaseSensitively;
    if (m_findInfo.matchWord)
        flag = flag | QTextDocument::FindWholeWords;
    if (m_findInfo.backwards)
        flag = flag | QTextDocument::FindBackward;

    QTextCursor c;

    if (m_findInfo.regex)
        c = ed->document()->find(QRegularExpression(m_findInfo.find), ed->textCursor(), flag);
    else
        c = ed->document()->find(m_findInfo.find, ed->textCursor(), flag);

    return c;
}

void MCE_MainWindow::on_actionSelect_All_triggered()
{
    m_tabInfo[m_tabBar->currentIndex()].widget->selectAll();
}

void MCE_MainWindow::on_actionCut_triggered()
{
    m_tabInfo[m_tabBar->currentIndex()].widget->cut();
}

void MCE_MainWindow::on_actionCopy_triggered()
{
    m_tabInfo[m_tabBar->currentIndex()].widget->copy();
}

void MCE_MainWindow::on_actionPaste_triggered()
{
    m_tabInfo[m_tabBar->currentIndex()].widget->paste();
}

void MCE_MainWindow::on_actionToolbar_triggered()
{
    if(!m_toolBarVisible)
    {
        ui->toolBar->setVisible(true);
        m_toolBarVisible = true;
    }
    else
    {
        ui->toolBar->setVisible(false);
        m_toolBarVisible = false;
    }

    ui->statusBar->showMessage(QIcon::themeName(), 3000);
}

void MCE_MainWindow::m_getNonce( QByteArray & str )
{
    std::mt19937_64 gen;
    gen.seed( CASTUINT64(QDateTime::currentMSecsSinceEpoch()) );
    for(int i = 0; i < 4; ++i)
    {
        uint64_t r = gen();
        for(int j = 0; j < 8; ++j)
            str[(i*8)+j] = CASTINT8( (r>>(8*j))&0xff );
    }
}

inline QByteArray MCE_MainWindow::m_getKeyString( QByteArray pwd )
{
    return QCryptographicHash::hash( pwd, QCryptographicHash::Sha3_256 );
}

inline bool MCE_MainWindow::m_getPassword( QByteArray & pwd)
{
    bool ok = false;
    pwd = QInputDialog::getText(
                nullptr, "Password",
                "Enter password for the file (empty password means no encryption):",
                QLineEdit::Password, "", &ok ).toUtf8();
    return ok;
}

void MCE_MainWindow::on_actionHelp_triggered()
{
    m_help.show();
}

void MCE_MainWindow::on_actionAbout_triggered()
{
    m_about.show();
}

void MCE_MainWindow::on_actionTheme_Preferences_triggered()
{
    m_themeDlg->show();
}

void MCE_MainWindow::onThemeSet()
{
    m_theme = m_themeDlg->getTheme();
    for(int i = 0; i < m_tabInfo.size(); ++i)
    {
       m_tabInfo[i].widget->setStyleSheet(m_theme.styleString());
    }

    m_setThemeAndSync();
    m_themeDlg->close();
}

void MCE_MainWindow::m_setThemeAndSync()
{
    m_settings->beginGroup("theme");
    m_settings->setValue("bgnd_color", m_theme.bgndClr);
    m_settings->setValue("fgnd_color", m_theme.fgndClr);
    m_settings->setValue("selb_color", m_theme.selBClr);
    m_settings->setValue("font_name",  m_theme.fntName);
    m_settings->setValue("font_size",  m_theme.fntSize);
    m_settings->setValue("font_style", m_theme.fntStyle);
    m_settings->endGroup();
    m_settings->sync();
}

void MCE_MainWindow::on_actionPrint_triggered()
{
    QPrinter printer;
    QPrintDialog printDlg( &printer, this );
    QTextEdit * ed = m_tabInfo[m_tabBar->currentIndex()].widget;
    printDlg.setWindowTitle( tr("Print Document") );
    if( ed->textCursor().hasSelection() )
        printDlg.addEnabledOption( QAbstractPrintDialog::PrintSelection );
    if( printDlg.exec() == QDialog::Accepted )
    {
        ed->print( &printer );
    }
}
