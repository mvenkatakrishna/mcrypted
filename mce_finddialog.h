/***********************************************************************
* Copyright (C) 2019  Mahesh Venkata Krishna (mahesh@maheshvk.com)   *
*                                                                      *
* This program is free software: you can redistribute it and/or modify *
* it under the terms of the GNU General Public License as published by *
* the Free Software Foundation, either version 3 of the License, or    *
* (at your option) any later version.                                  *
*                                                                      *
* This program is distributed in the hope that it will be useful,      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of       *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
* GNU General Public License for more details.                         *
*                                                                      *
* You should have received a copy of the GNU General Public License    *
* along with this program. If not, see <https://www.gnu.org/licenses/> *
***********************************************************************/
#ifndef FINDDIALOG_H
#define FINDDIALOG_H

#include <QDialog>

namespace Ui {
class FindDialog;
}

enum MCE_Button
{
    NEXT,
    REPLACE,
    REPLACEALL
};

struct MCE_FindInfo
{
    MCE_Button btn = NEXT;
    QString find = "";
    QString replace = "";
    bool caseSensitive = false;
    bool matchWord = false;
    bool backwards = false;
    bool regex = false;
};

class MCE_FindDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MCE_FindDialog(QWidget *parent = nullptr);
    ~MCE_FindDialog();

    MCE_FindInfo getInfo();

private slots:
    void on_closeBtn_clicked();

    void on_nextBtn_clicked();

    void on_replaceBtn_clicked();

    void on_repAllBtn_clicked();

signals:
    void btnClicked();
    void findDlgClosed();

private:
    inline void m_readGuiInfo();

    Ui::FindDialog *ui;

    MCE_FindInfo m_info;
};

#endif // FINDDIALOG_H
